﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    private float startingZ;
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        startingZ = this.transform.position.z;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, startingZ);   
    }
}
