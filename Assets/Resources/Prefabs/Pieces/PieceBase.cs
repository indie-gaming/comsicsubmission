﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceBase : MonoBehaviour
{
    private FieldManager fm;


    public enum Team
    {
        independant,
        yellow,
        green,
        red,
        blue,
        purple
    }

    [Header("Piece Team")]
    [SerializeField]
    public Team allegiance;

    // Target and movement variables
    private GameObject _target = null;
    private List<Vector3> _path;

    public virtual void Start()
    {
        fm = GameObject.Find("Grid").GetComponent<FieldManager>();
    }

    // Update is called once per frame
    public virtual void Update()
    {
        HandleActions();
    }


    public virtual void HandleActions()
    {
        if (_target == null) // if no target
        {

        }
        else
        {

        }
    }

}
