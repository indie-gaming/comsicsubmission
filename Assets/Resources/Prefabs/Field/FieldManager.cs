﻿using DotNetty.Common.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class FieldManager : MonoBehaviour
{
    [Header("Field Tile and Map")]
    public TileBase fieldTile;
    public Tilemap fieldTilemap;

    private Vector3Int _previousHoverTile;
    private List<Vector3Int> tileCenterPosGizmos = new List<Vector3Int>();
    private List<PieceBase> _pieceList = new List<PieceBase>();
    private BoundsInt _fieldSize = new BoundsInt(new Vector3Int(10, 10, 0), new Vector3Int(20, 20, 1));

    private void Update()
    {
        HandleTileHover();
    }


    public void AddPiece(PieceBase piece)
    {
        // add piece and centre it onto grid cell
    }

    /// <summary>
    /// Handles what happens when mouse is hovering over a tile;
    /// </summary>
    public void HandleTileHover()
    {
        Vector3Int? cellPos = TilemapUtils.CellUnderMouseInMap(fieldTilemap);
        if (cellPos != null && !cellPos.Equals(_previousHoverTile))
        {
            fieldTilemap.SetTileFlags((Vector3Int)_previousHoverTile, TileFlags.None);
            fieldTilemap.SetColor(_previousHoverTile, Color.white);
            fieldTilemap.SetTileFlags((Vector3Int)cellPos, TileFlags.None);
            fieldTilemap.SetColor((Vector3Int)cellPos, Color.blue);
            if (!tileCenterPosGizmos.Contains((Vector3Int)cellPos))
                tileCenterPosGizmos.Add((Vector3Int)cellPos);
            _previousHoverTile = (Vector3Int)cellPos;
        }
    }


    /// <summary>
    /// Draw markers for scene view
    /// </summary>
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(_fieldSize.position, _fieldSize.size);
        foreach (Vector3Int pos in tileCenterPosGizmos)
        {
            Gizmos.DrawSphere(fieldTilemap.GetCellCenterLocal(pos), 0.5f);
        }
    }
}

public static class AStarGridSearch
{
    public static List<Vector3Int> GetPath(Tilemap map, Vector3Int startCell, Vector3Int endCell, List<TileBase> blackListTiles, List<TileBase> whiteListTiles)
    {
        PriorityQueue<CellValuePair> queue = new PriorityQueue<CellValuePair>();
        List<CellValuePair> searchedNodes = new List<CellValuePair>();
        queue.Enqueue(new CellValuePair(startCell, ))
        bool destinationReached = false;

        while (!destinationReached)
            /*
           consider the node with the lowest f score in the open list
           if (this node is our destination node) :
               we are finished
           if not:
               put the current node in the closed list and look at all of its neighbors
               for (each neighbor of the current node):
                   if (neighbor has lower g value than current and is in the closed list) :
                       replace the neighbor with the new, lower, g value
                       current node is now the neighbor's parent            
                   else if (current g value is lower and this neighbor is in the open list ) :
                       replace the neighbor with the new, lower, g value
                       change the neighbor's parent to our current node

                   else if this neighbor is not in both lists:
                       add it to the open list and set its g
            */
        return null;
    }

    public static List<Vector3Int> GetPath(Tilemap map,Vector3Int startCell, Vector3Int endCell)
    {
        return GetPath(map, startCell, endCell, new List<TileBase>(), new List<TileBase>());
    }


    // implementation for floating-point  Manhattan Distance
    public static float ManhattanDistance(Vector3Int cell1, Vector3Int cell2)
    {
        return Math.Abs(cell1.x - cell2.x) + Math.Abs(cell1.y - cell2.y);
    }

    /// <summary>
    /// Gets the EuclideanDistance between two points
    /// </summary>
    /// <param name="cell1">first point</param>
    /// <param name="cell2">second point</param>
    /// <returns></returns>
    public static float EuclideanDistance(Vector3Int cell1, Vector3Int cell2)
    {
        float square = (cell1.x - cell2.x) * (cell1.x - cell2.x) + (cell1.y - cell2.y) * (cell1.y - cell2.y);
        return Mathf.Sqrt(square);
    }


    // implementation for floating-point Chebyshev Distance
    public static float ChebyshevDistance(Vector3Int cell1, Vector3Int cell2)
    {
        // not quite sure if the math is correct here
        return Math.Max(Math.Abs(cell2.x - cell1.x), Math.Abs(cell2.y - cell1.y));
    }
}


/// <summary>
/// Cell Value pair for A* algorithym
/// </summary>
public class CellValuePair : IComparable, IComparer
{
    private Vector3Int _cell;
    private float _distance;

    public CellValuePair(Vector3Int cell, float distance)
    {
        _cell = cell;
        _distance = distance;
    }

    public float Distance
    {
        get { return _distance; }
    }
    public Vector3Int Cell
    {
        get { return _cell; }
    }

    public int Compare(object x, object y)
    {
        CellValuePair c1 = (CellValuePair)x;
        CellValuePair c2 = (CellValuePair)y;
        if (c1.Distance > c2.Distance)
            return 1;
        if (c1.Distance < c2.Distance)
            return -1;
        else
            return 0;
    }

    public int CompareTo(object obj)
    {
        CellValuePair c = (CellValuePair)obj;
        return Compare(this, c);
    }
}